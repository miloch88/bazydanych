package mySQL.filmy24h;

import java.sql.*;
import java.util.Scanner;

public class filmy24h {

    private static Connection connection = null;
    private static Statement st = null;
    private static ResultSet rs = null;

    public static void addNew() {

        try {
            Scanner scanner = new Scanner(System.in);
            connection = DriverManager.getConnection("jdbc:mysql://localhost/filmy24h", "root", "miloch");
            PreparedStatement pst = null;

            String stm = "INSERT INTO uzytkownik(uzytkownik_imie,uzytkownik_nazwisko ) VALUES(?,?)";
            pst = connection.prepareStatement(stm);

            System.out.println("Podaj swoje imię: ");
            String one = scanner.nextLine();
            System.out.println("Podaj swoje nazwisko: ");
            String two = scanner.nextLine();

            pst.setString(1, one);
            pst.setString(2, two);

            pst.executeUpdate();

        } catch (SQLException e) {

            System.out.println("Blad polaczenia do bazy danych");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("==Tu mozna dodac operacje na DB==");
        } else {
            System.out.println("Polaczenie nieudane");
        }
    }

    public static void searchMovie() {

        try {

            Scanner scanner = new Scanner(System.in);
            connection = DriverManager.getConnection("jdbc:mysql://localhost/filmy24h", "root", "miloch");
            PreparedStatement pst = null;

            System.out.println("Podaj frazę: ");
            String one = scanner.nextLine();

            st = connection.createStatement();
            pst = connection.prepareStatement("SELECT * FROM film WHERE tytul LIKE '" + one + "%'");

            rs = pst.executeQuery();

            while (rs.next()) {
                System.out.print(rs.getInt(1));
                System.out.print(": ");
                System.out.println(rs.getString(2));
            }

        } catch (SQLException e) {

            System.out.println("Blad polaczenia do bazy danych");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("==Tu mozna dodac operacje na DB==");
        } else {
            System.out.println("Polaczenie nieudane");
        }
    }

    private static void downloadMovie() {

        try {

            Scanner scanner = new Scanner(System.in);
            connection = DriverManager.getConnection("jdbc:mysql://localhost/filmy24h", "root", "miloch");
            PreparedStatement pst = null;

            System.out.println("Podaj swoje id: ");
            int id = scanner.nextInt();

            st = connection.createStatement();
            pst= connection.prepareStatement("SELECT * FROM film");
            rs = pst.executeQuery();

            while (rs.next()) {
                System.out.printf(String.valueOf(rs.getInt(1)));
                System.out.print(": ");
                System.out.println(rs.getString(2));

            }

            System.out.println("Podaj numer filmu: ");
            int one = scanner.nextInt();

            pst = connection.prepareStatement("SELECT * FROM film WHERE film_id = (SELECT film_id FROM film WHERE film_id ='" + one + "')");
            rs = pst.executeQuery();

            while (rs.next()) {
                System.out.print(rs.getInt(1));
                System.out.print(": ");
                System.out.println(rs.getString(6));
            }

            pst = connection.prepareStatement("INSERT INTO historia (id, film) VALUES (?,?)");

            pst.setInt(1, id);
            pst.setInt(2,id);

            pst.executeUpdate();


        } catch (SQLException e) {

            System.out.println("Blad polaczenia do bazy danych");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("==Tu mozna dodac operacje na DB==");
        } else {
            System.out.println("Polaczenie nieudane");
        }

    }

    public static void main(String[] argv) {

        //Testowanie połączenia
        System.out.println("----Test polaczenia do bazy MySQL --------");
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {

            System.out.println("Nie znaleziono drivera MySQL");
            e.printStackTrace();
            return;
        }
        System.out.println("MySQL JDBC Driver zarejestrowany");

        Connection connection = null;

        System.out.println("Co chcesz zrobić?");
        System.out.println("1. Rejestracja nowego użytkownika: ");
        System.out.println("2. Wyszukać filmu: ");
        System.out.println("3. Pobrać film: ");

        Scanner scanner = new Scanner(System.in);
        int choose = scanner.nextInt();

        switch (choose) {
            case (1):
                addNew();
                break;
            case (2):
                searchMovie();
                break;
            case(3):
                downloadMovie();
                break;
        }

    }


}